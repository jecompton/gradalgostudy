def fib(n):
    '''
    Calculate Fib using dynamic programming

    This version is from https://www.geeksforgeeks.org/program-for-nth-fibonacci-number/
    but I don't trust it since it dies on max recur on 1000. I don't think this
    would happen on plain tail recur.
    '''
    fibos = [0, 1]

    # init array
    while len(fibos) < n + 1:
        fibos.append(0)

    if n <= 1:
        return n
    else:
        if fibos[n - 1] == 0:
            fibos[n -1] = fib(n - 1)
        if fibos[n - 2] == 0:
            fibos[n - 2] = fib(n - 2)

    fibos[n] = fibos[n - 2] + fibos[n - 1]
    return fibos[n]
