import fibDP01 as fib


def test_fib_zero_is_zero():
    ''' test that fib(0) == 0 '''
    assert fib.fib(0) == 0


def test_fib_one_is_one():
    ''' test that fib(1) == 1 '''
    assert fib.fib(1) == 1


def test_fib_two_is_one():
    ''' test that fib(2) == 1 '''
    assert fib.fib(2) == 1


def test_fib_three_is_two():
    ''' test that fib(3) == 2 '''
    assert fib.fib(3) == 2


def test_fib_four_is_three():
    ''' test that fib(4) == 3 '''
    assert fib.fib(4) == 3


def test_fib_five_is_five():
    ''' test that fib(5) == 5 '''
    assert fib.fib(5) == 5


def test_fib_twenty_is_6765():
    ''' test that fib(20) == 6765 '''
    assert fib.fib(20) == 6765


def test_fib_1000_is_big():
    '''
        test that fib(1000) == big num.

        The real test here is to see if you really have it memoizing. With pure
        recursion, it breaks Python.
    '''
    big_num = 43466557686937456435688527675040625802564660517371780402481729089536555417949051890403879840079255169295922593080322634775209689623239873322471161642996440906533187938298969649928516003704476137795166849228875
    assert fib.fib(1000) == big_num
