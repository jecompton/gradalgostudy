## Intro

Here are some notes on our study for intro to grad. algorithms.


## Links & Resources

- Official prof page for GA: https://gt-algorithms.com/
- Previous grad algo class resources: http://omscs.wikidot.com/courses:cs6505
- Current grad algo resources: http://omscs.wikidot.com/courses:cs6515
- Class udacity: https://classroom.udacity.com/courses/ud401
- Class textbook: http://algorithmics.lsi.upc.edu/docs/Dasgupta-Papadimitriou-Vazirani.pdf

Various books on discrete math and algorithms are useful. The Cormen book (CLRS)
was used in the past:

https://www.amazon.com/Introduction-Algorithms-3rd-MIT-Press/dp/0262033844/ref=sr_1_2?keywords=clrs+algorithms+book&qid=1581784327&sr=8-2


## Exercises

https://blog.usejournal.com/top-50-dynamic-programming-practice-problems-4208fed71aa3?gi=653438a4a3f1


## Topics to master

- dynamic programming
  - FIB
  - LIS
  - LCS
  - Knapsack
  - Chain multiply
  - Shortest path on graphs
- randomized algorithms
  - modular arithmetic
  - fast modular exponentiation
  - multiplicative inverses
  - Fermat's little theorem
  - RSA
  - primality testing
  - bloom filters
- divide and conquer
  - multiplying larget integers
  - linear time median for unsorted list
  - solving recurrences
  - fast fourier transform
  - complex roots of unity
  - polynomial multiplication
  - inverse FFT
- graph algorithms
  - connectivity of undirected graphs
  - topological sorting of a DAG
  - strongly connected components of general directed graphs
  - 2-SAT w/ SCC
  - Minimum Spanning tree (MST)
  - Markov Chains
  - PageRank
- max-flow
  - residual network
  - Ford-Fulkerson
  - min-cut theorem
  - image segmentation
  - Edmonds-Karp augmenting path
  - generalization allowing demand constraints
- linear programming
  - basics, standard form
  - simplex algorithm
  - geometry of linear programs
    - feasible region
    - infeasible LPs
    - unbounded LPs
  - duality
    - convertin LP to its dual
    - weak and strong duality
  - Max-SAT
  - Integer programming
  - Calculus
- NP-completeness
  - definition
  - proving a problem is NP
  - 3SAT
  - graphs and NP
  - Knapsack and NP
  - Halting is undecidable
